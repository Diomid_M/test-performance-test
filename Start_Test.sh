#!/bin/bash
echo "Enter servers password:"
read -r ServPass;
SERVERS_ADDRESS=(192.168.7.157 192.168.7.60 192.168.7.61 192.168.7.62)
SERVER_MAIN=192.168.7.158
echo "Enter the number of users (x4): 300"
read -r USERS;
echo "Enter target host link: qa.sasomange.rs"
read -r SITE_PATH;
echo "Test duration (sec): 1800"
read -r DURATION
# Start Jmeter Servers
for IP_AD in ${SERVERS_ADDRESS[*]}; do
  screen -dmS JmeterServer_"$IP_AD" bash -c "sshpass -p \"$ServPass\" ssh qa@\"$IP_AD\" ' cd /opt/sasomange-performance/jmeter/projects/saso-mange ; ./startJMeterServer.sh'"
done
# Run ant, start test (if test will started from Main server, disable this part of code (ssh connection to main): "sshpass -p \"$ServPass\" ssh qa@\"$SERVER_MAIN\")
screen -S JmeterMain bash -c "sshpass -p \"$ServPass\" ssh qa@\"$SERVER_MAIN\" 'sleep 5; \
cd /opt/sasomange-performance/jmeter; \
ant test \
  -Dusers=\"${USERS:-300}\" \
  -Dname=QA-sasomange \
  -Dtest=./projects/saso-mange/SasoMangeTestPlan.jmx \
  -Dduration=\"${DURATION:-1800}\" \
  -Dreport=true \
  -Drampup=60 \
  -DthinkTime=15000 \
  -DdelayOffset=5000 \
  -DpagesToBrowse=0 \
  -DitemsAddToCart=0 \
  -DtimesToSearch=0 \
  -DbrowserPercent=90 \
  -DcartPercent=0 \
  -DcheckoutPercent=10 \
  -DtargetHost=\"${SITE_PATH:='qa.sasomange.rs'}\" \
  -Drunremote=true; \
   sleep 5'"
# Kill Jmeter Servers
for IP_AD in ${SERVERS_ADDRESS[*]}; do
  screen -dmS JmeterServer_KILL_"$IP_AD" bash -c "sshpass -p \"$ServPass\" ssh qa@\"$IP_AD\" pkill java"
done