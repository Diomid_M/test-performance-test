<?xml version="1.0"?>
<xsl:stylesheet 
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
>
<xsl:output method="html" indent="yes" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" />

<!-- Defined parameters (overrideable) -->
<xsl:param    name="showData" select="'n'"/>
<xsl:param    name="titleReport" select="'Load Test Results'"/>
<xsl:param    name="dateReport" select="'date not defined'"/>
<xsl:param    name="maxThreads" select="'max threads not defined'"/>
<xsl:param    name="maxThroughput" select="'max throughput not defined'"/>
<xsl:param    name="maxPageViews" select="'max pageviews not defined'"/>
<xsl:param    name="rampup" select="'rampup not defined'"/>
<xsl:param    name="thinkTime" select="'thinkTime not defined'"/>
<xsl:param    name="pagesToBrowse" select="'pagesToBrowse not defined'"/>
<xsl:param    name="itemsAddToCart" select="'itemsAddToCart not defined'"/>
<xsl:param    name="timesToSearch" select="'timesToSearch not defined'"/>
<xsl:param    name="browserPercent" select="'browserPercent not defined'"/>
<xsl:param    name="cartPercent" select="'cartPercent not defined'"/>
<xsl:param    name="checkoutPercent" select="'checkoutPercent not defined'"/>

<xsl:key name="pageLabels" match="httpSample" use="@lb"/>
<xsl:key name="pageLabelsSuccessful" match="httpSample" use="concat(@lb, '-', @s)"/>

<xsl:template match="testResults">
	<html>
		<head>
			<title><xsl:value-of select="$titleReport" /></title>
			<style type="text/css">
				body {
					font:normal 68% verdana,arial,helvetica;
					color:#000000;
				}
				table tr td, table tr th {
					font-size: 68%;
				}
				table.details tr th{
				    color: #ffffff;
					font-weight: bold;
					text-align:center;
					background:#2674a6;
					white-space: nowrap;
				}
				table.details tr td{
					background:#eeeee0;
					white-space: nowrap;
				}
				
				td.header {
					color: #ffffff;
					font-weight: bold;
					text-align:left;
					background:#2674a6 !important;
					white-space: nowrap;
				}
				h1 {
					margin: 0px 0px 5px; font: 165% verdana,arial,helvetica
				}
				h2 {
					margin-top: 1em; margin-bottom: 0.5em; font: bold 125% verdana,arial,helvetica
				}
				h3 {
					margin-bottom: 0.5em; font: bold 115% verdana,arial,helvetica
				}
				.Failure {
					font-weight:bold; color:red;
				}
				
	
				img
				{
				  border-width: 0px;
				}
				
				.expand_link
				{
				   position=absolute;
				   right: 0px;
				   width: 27px;
				   top: 1px;
				   height: 27px;
				}
				
				.page_details
				{
				   display: none;
				}
                                
                .page_details_expanded
                {
                    display: block;
                    display/* hide this definition from  IE5/6 */: table-row;
                }
			</style>
			<script><![CDATA[
               function expand(details_id)
			   {
			      
			      document.getElementById(details_id).className = "page_details_expanded";
			   }
			   
			   function collapse(details_id)
			   {
			      
			      document.getElementById(details_id).className = "page_details";
			   }
			   
			   function change(details_id)
			   {
			      if(document.getElementById(details_id+"_image").src.match("expand"))
			      {
			         document.getElementById(details_id+"_image").src = "collapse.png";
			         expand(details_id);
			      }
			      else
			      {
			         document.getElementById(details_id+"_image").src = "expand.png";
			         collapse(details_id);
			      } 
               }
			]]></script>

			<script><![CDATA[
			function sortTable(table, col, reverse) {
			    var tb = table.tBodies[0], // use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
			        tr = Array.prototype.slice.call(tb.rows, 0), // put rows into array
			        i;
			    reverse = -((+reverse) || -1);
			    istext = isNaN(tr[0].cells[col].textContent.trim());
			    tr = tr.sort(function (a, b) { // sort rows
			        return reverse // `-1 *` if want opposite order
			            * (istext ? (a.cells[col].textContent.trim() // using `.textContent.trim()` for test
			                        .localeCompare(b.cells[col].textContent.trim())) 
			                      :
			              			( a.cells[col].textContent.trim() - b.cells[col].textContent.trim())
              			  );
			    });
			    for(i = 0; i < tr.length; ++i) tb.appendChild(tr[i]); // append each row in order
			}
				
			function makeSortable(table) {
			    var th = table.tHead, i;
			    th && (th = th.rows[0]) && (th = th.cells);
			    if (th) i = th.length;
			    else return; // if no `<thead>` then do nothing
			    while (--i >= 0) (function (i) {
			        var dir = 1;
			        th[i].addEventListener('click', function () {sortTable(table, i, (dir = 1 - dir))});
			    }(i));
			}
			]]></script>

		</head>
		<body onload="makeSortable(document.getElementById('pagelist'));">

			<xsl:message>Processing pageHeader</xsl:message>
			<xsl:call-template name="pageHeader" />
		
			<xsl:message>Processing summary</xsl:message>
			<xsl:call-template name="summary" />
			<br/><br/><br/><br/>
			
			<xsl:message>Processing pageList</xsl:message>
			<xsl:call-template name="pagelist" />
			<br/><br/><br/><br/>
			
			<xsl:message>Processing graphs</xsl:message>
			<xsl:call-template name="graphs" />
			<br/><br/><br/><br/>
	
			<xsl:message>Processing detail</xsl:message>
			<xsl:call-template name="detail" />
			<br/><br/><br/><br/>
			
				
		</body>
	</html>
</xsl:template>

<xsl:template name="pageHeader">
	<h1><xsl:value-of select="$titleReport" /></h1>
	<table width="100%">
		<tr>
			<td align="left">Date report: <xsl:value-of select="$dateReport" /></td>
		</tr>
	</table>
	<hr size="1" />
</xsl:template>

<xsl:template name="summary">
			<xsl:variable name="allCount" select="count(/testResults/*)" />
			<xsl:variable name="mainCount" select="count(/testResults/httpSample/@lb[contains(.,'main')])" />
			<xsl:variable name="allFailureCount" select="count(/testResults/*[attribute::s='false'])" />
			<xsl:variable name="allSuccessCount" select="count(/testResults/*[attribute::s='true'])" />
			<xsl:variable name="allSuccessPercent" select="$allSuccessCount div $allCount" />
			<xsl:variable name="allTotalTime" select="sum(/testResults/*/@t)" />
			<xsl:variable name="allBytes" select="sum(/testResults/*/@by)" />
			<xsl:variable name="allAverageTime" select="$allTotalTime div $allCount" />
			<xsl:variable name="all95thPercentileTime" > 
				<xsl:call-template name="percentile">
					<xsl:with-param name="nodes" select="/testResults/*/@t" />
					<xsl:with-param name="pct" select="0.95" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="all99thPercentileTime" > 
				<xsl:call-template name="percentile">
					<xsl:with-param name="nodes" select="/testResults/*/@t" />
					<xsl:with-param name="pct" select="0.99" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="allStartTime">
				<xsl:call-template name="min">
					<xsl:with-param name="nodes" select="/testResults/*/@ts" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="allEndTime">
				<xsl:call-template name="max">
					<xsl:with-param name="nodes" select="/testResults/*/@ts" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="allRunningTime" select="($allEndTime - $allStartTime) div 1000"/>
			<xsl:variable name="allKBytesTime" select="($allBytes div 1024) div ($allRunningTime)" />
			<xsl:variable name="allAverageRequestsPerSecond" select="$allCount div $allRunningTime" />
			<xsl:variable name="allAveragePagesPerSecond" select="$mainCount div $allRunningTime" />
			<xsl:variable name="allMinTime">
				<xsl:call-template name="min">
					<xsl:with-param name="nodes" select="/testResults/*/@t" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="allMaxTime">
				<xsl:call-template name="max">
					<xsl:with-param name="nodes" select="/testResults/*/@t" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="allUsers">
				<xsl:call-template name="max">
					<xsl:with-param name="nodes" select="/testResults/*/@na" />
				</xsl:call-template>
			</xsl:variable>
			<xsl:attribute name="class">
				<xsl:choose>
					<xsl:when test="$allFailureCount &gt; 0">Failure</xsl:when>
				</xsl:choose>
			</xsl:attribute>
<div>	

<table border="0" width="95%">
<tr><td valign="top">
		
	<h2>KPIs</h2>
	<table class="details" border="0" cellpadding="5" cellspacing="2" width="300">
	<tr>
		<td class="header">Maximum Users</td>
		<td align="right">
			<xsl:value-of select="$maxThreads" />
		</td>
	</tr>
	<tr>
		<td class="header">Max req/s</td>
		<td align="right">
			<xsl:value-of select="$maxThroughput" />
		</td>
	</tr>
	<tr>
		<td class="header">Average req/s</td>
		<td align="right">
		<xsl:call-template name="display-2dp">
			<xsl:with-param name="value" select="$allAverageRequestsPerSecond" />
		</xsl:call-template>
		</td>
	</tr>

	<tr>
		<td class="header">Max PV/s</td>
		<td align="right">
			<xsl:value-of select="$maxPageViews" />
		</td>
	</tr>
	<tr>
		<td class="header">Average PV/s</td>
		<td align="right">
			<xsl:call-template name="display-2dp">
				<xsl:with-param name="value" select="$allAveragePagesPerSecond" />
			</xsl:call-template>
		</td>
	</tr>
	<tr>
		<td class="header">Average Response Time (ms)</td>
		<td align="right">
		<xsl:call-template name="display-time">
					<xsl:with-param name="value" select="$allAverageTime" />
		</xsl:call-template>
		</td>
	</tr>
	<tr>
		<td class="header">95th Percentile Response Time (ms)</td>
		<td align="right">
		<xsl:call-template name="display-time">
					<xsl:with-param name="value" select="$all95thPercentileTime" />
		</xsl:call-template>
		</td>
	</tr>
	<tr>
		<td class="header">99th Percentile Response Time (ms)</td>
		<td align="right">
		<xsl:call-template name="display-time">
					<xsl:with-param name="value" select="$all99thPercentileTime" />
		</xsl:call-template>
		</td>
	</tr>
	<tr>
		<td class="header">Success Rate</td>
		<td align="right">
		<xsl:call-template name="display-percent">
			<xsl:with-param name="value" select="$allSuccessPercent" />
		</xsl:call-template>
	</td>
	</tr>
	</table>
</td>
<td><br></br></td>
<td valign="top">
	<h2>Summary</h2>
	<table class="details" border="0" cellpadding="5" cellspacing="2" width="300">
		<tr valign="top">
			<td class="header">Running Time (s)</td>
			<td align="right">
			<xsl:call-template name="display-2dp">
				<xsl:with-param name="value" select="$allRunningTime" />
			</xsl:call-template>
			</td>
		</tr>
		<tr valign="top">
			<td class="header">Bytes Downloaded</td>
			<td align="right">
				<xsl:value-of select="$allBytes" />
			</td>
		</tr>
		<tr valign="top">
			<td class="header">Bandwidth (KB/s)</td>
			<td align="right">
				<xsl:call-template name="display-2dp">
					<xsl:with-param name="value" select="$allKBytesTime" />
				</xsl:call-template>
			</td>
		</tr>
		<tr>
			<td class="header"># Samples</td>
			<td align="right">
				<xsl:value-of select="$allCount" />
			</td>
		</tr>
		<tr>
			<td class="header">Failures</td>
			<td align="right">
				<xsl:value-of select="$allFailureCount" />
			</td>
		</tr>
		<tr>
			<td class="header">Max Time (ms)</td>
			<td align="right">
				<xsl:call-template name="display-time">
					<xsl:with-param name="value" select="$allMaxTime" />
				</xsl:call-template>
			</td>
		</tr>
		<tr>
			<td class="header">Min Time (ms)</td>
			<td align="right">
				<xsl:call-template name="display-time">
					<xsl:with-param name="value" select="$allMinTime" />
				</xsl:call-template>
			</td>
		</tr>
	</table>
</td>
<td><br></br></td>
<td valign="top">
	<h2>Test Profile Configuration</h2>
<xsl:if test="$rampup != '${rampup}'">
	<table class="details" border="0" cellpadding="5" cellspacing="2" width="300">
			<tr>
			<td class="header">Ramp up (s)</td>
			<td align="right"><xsl:value-of select="$rampup" /></td>
		</tr>
		<tr>
			<td class="header">Think Time (ms)</td>
			<td align="right"><xsl:value-of select="$thinkTime" /></td>
		</tr>
		<tr>
			<td class="header">Pages to Browse</td>
			<td align="right"><xsl:value-of select="$pagesToBrowse" /></td>
		</tr>
		<tr>
			<td class="header">Items to cart</td>
			<td align="right"><xsl:value-of select="$itemsAddToCart" /></td>
		</tr>
		<tr>
			<td class="header">Times to Search</td>
			<td align="right"><xsl:value-of select="$timesToSearch" /></td>
		</tr>
		<tr>
			<td class="header">% of Users Browsing</td>
			<td align="right"><xsl:value-of select="$browserPercent*100" /></td>
		</tr>
		<tr>
			<td class="header">% of Users Adding to Cart</td>
			<td align="right"><xsl:value-of select="$cartPercent*100" /></td>
		</tr>
		<tr>
			<td class="header">% Conversion Rate</td>
			<td align="right"><xsl:value-of select="$checkoutPercent*100" /></td>
		</tr>
	</table>
</xsl:if>
<xsl:if test="$rampup = '${rampup}'">
<br></br>
This report was generated with ant createreport.
<br></br>
The Test Profile configuration was sent as a parameter of the test execution (ant test).
<br></br>
Please check the configuration from there.
</xsl:if>
</td>
</tr>
</table>

</div>

</xsl:template>

<xsl:template name="pagelist">
<div>
	<h2>Pages</h2>
	<table id="pagelist" class="details" border="0" cellpadding="5" cellspacing="2" width="95%">
		<thead valign="top">
			<th>URL</th>
			<th>Average PI/s</th>
			<th>Average Response Time (ms)</th>
			<th>95th Percentile Response Time (ms)</th>
			<th>99th Percentile Response Time (ms)</th>
			<th>Max Time</th>
			<th>Min Time</th>
			<th># Samples</th>
			<th>Failures</th>
			<th>Success Rate</th>
		</thead>
		
		<xsl:for-each select="httpSample[generate-id() =  generate-id(key('pageLabels', @lb)[1])]">
		      
			  <xsl:variable name="label" select="current()/@lb" />  
			  <xsl:variable name="count" select="count(key('pageLabels', @lb))" />
			  <xsl:variable name="totalTime" select="sum(key('pageLabels', @lb)/@t)" />
			  <xsl:variable name="averageTime" select="$totalTime div $count" />
			  <xsl:variable name="ninetyFifthPctTime" >
			      <xsl:call-template name="percentile">
					  <xsl:with-param name="nodes" select="key('pageLabels', @lb)/@t" />
					  <xsl:with-param name="pct" select="0.95" />
				  </xsl:call-template>
			  </xsl:variable>
			  <xsl:variable name="ninetyNinethPctTime" >
			      <xsl:call-template name="percentile">
					  <xsl:with-param name="nodes" select="key('pageLabels', @lb)/@t" />
					  <xsl:with-param name="pct" select="0.99" />
				  </xsl:call-template>
			  </xsl:variable>
			  
    			<xsl:variable name="startTime">
    				<xsl:call-template name="min">
    					<xsl:with-param name="nodes" select="key('pageLabels', @lb)/@ts" />
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:variable name="endTime">
    				<xsl:call-template name="max">
    					<xsl:with-param name="nodes" select="key('pageLabels', @lb)/@ts" />
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:variable name="runningTime" select="($endTime - $startTime) div 1000"/>
    			<xsl:variable name="averageRequestsPerSecond" select="$count div $runningTime" />
    			<xsl:variable name="minTime">
    				<xsl:call-template name="min">
    					<xsl:with-param name="nodes" select="key('pageLabels', @lb)/@t" />
    				</xsl:call-template>
    			</xsl:variable>
    			<xsl:variable name="maxTime">
    				<xsl:call-template name="max">
    					<xsl:with-param name="nodes" select="key('pageLabels', @lb)/@t" />
    				</xsl:call-template>
    			</xsl:variable>
				
	  			<xsl:variable name="failureCount" select="count(key('pageLabelsSuccessful', concat(@lb,'-','false')))" />
	  			<xsl:variable name="successCount" select="count(key('pageLabelsSuccessful', concat(@lb,'-','true')))" />
				<xsl:variable name="successPercent" select="$successCount div $count" />
	
  			<tr valign="top">
  				<td>
  					<xsl:value-of select="$label" />
  				</td>
  				<td align="center">
  				<xsl:call-template name="display-2dp">
  					<xsl:with-param name="value" select="$averageRequestsPerSecond" />
  				</xsl:call-template>
  				</td>
  				<td align="right">
  					<xsl:call-template name="display-time">
  						<xsl:with-param name="value" select="$averageTime" />
  					</xsl:call-template>
  				</td>
  				<td align="right">
  					<xsl:call-template name="display-time">
  						<xsl:with-param name="value" select="$ninetyFifthPctTime" />
  					</xsl:call-template>
  				</td>
  				<td align="right">
  					<xsl:call-template name="display-time">
  						<xsl:with-param name="value" select="$ninetyNinethPctTime" />
  					</xsl:call-template>
  				</td>
  				<td align="right">
  					<xsl:call-template name="display-time">
  						<xsl:with-param name="value" select="$maxTime" />
  					</xsl:call-template>
  				</td>
  				<td align="right">
  					<xsl:call-template name="display-time">
  						<xsl:with-param name="value" select="$minTime" />
  					</xsl:call-template>
  				</td> 
  				<td align="center">
  					<xsl:value-of select="$count" />
  				</td> 
  				<td align="center">
  					<xsl:value-of select="$failureCount" />
  				</td>
  				<td align="right">
  					<xsl:call-template name="display-percent">
  						<xsl:with-param name="value" select="$successPercent" />
  					</xsl:call-template>
  				</td>
  			</tr>
		 </xsl:for-each>
	</table>
	</div>
</xsl:template>

<xsl:template name="graphs">
<div>
	<h2>Graphs</h2>
	<table border="0" cellpadding="5" cellspacing="2" width="95%">
	<tr>
		<td><img src="graphs/AggregateChart-Median.png" /></td>
		<td><img src="graphs/AggregateChart-Avg.png" /></td>
	</tr>
	<tr>
		<td><img src="graphs/AggregateChart-90.png" /></td>
		<td><img src="graphs/AggregateChart-95.png" /></td>
	</tr>
	<tr>
		<td><img src="graphs/AggregateChart-99.png" /></td>
		<td><img src="graphs/ResponseTimeVsUsersLineChart.png" /></td>
	</tr>
	<tr>
		<td><img src="graphs/ThoughputVsUsersLineChart.png" /></td>
		<td><img src="graphs/BandwidthVsUsersLineChart.png" /></td>
	</tr>
	</table>
</div>
</xsl:template>

<xsl:template name="detail">
	<xsl:variable name="allFailureCount" select="count(/testResults/*[attribute::s='false'])" />

	<xsl:if test="$allFailureCount > 0">
		<h2>Failure Detail</h2>

		<xsl:for-each select="httpSample[generate-id() =  generate-id(key('pageLabels', @lb)[1])]">

			<xsl:variable name="failureCount" select="count(key('pageLabelsSuccessful', concat(@lb,'-','false')))" />
  			
			<xsl:if test="$failureCount > 0">
				<h3><xsl:value-of select="@lb" /><a><xsl:attribute name="name"><xsl:value-of select="@lb" /></xsl:attribute></a></h3>

				<table class="details" border="0" cellpadding="5" cellspacing="2" width="95%">
				<tr valign="top">
					<th>Response</th>
					<th>Failure Message</th>
					<xsl:if test="$showData = 'y'">
					   <th>Response Data</th>
					</xsl:if>
				</tr>
			
				<xsl:for-each select="key('pageLabelsSuccessful', concat(@lb,'-','false'))">
					<tr>
						<td><xsl:value-of select="@rc | @rs" /></td>
						<td><xsl:value-of select="@rm" />
						<xsl:value-of select="assertionResult/failureMessage" /></td>
						<xsl:if test="$showData = 'y'">
							<td><xsl:value-of select="./binary" /></td>
						</xsl:if>
					</tr>
				</xsl:for-each>
				
				</table>
			</xsl:if>

		</xsl:for-each>
	</xsl:if>
</xsl:template>

<xsl:template name="min">
	<xsl:param name="nodes" select="/.." />
	<xsl:choose>
		<xsl:when test="not($nodes)">NaN</xsl:when>
		<xsl:otherwise>
			<xsl:for-each select="$nodes">
				<xsl:sort data-type="number" />
				<xsl:if test="position() = 1">
					<xsl:value-of select="number(.)" />
				</xsl:if>
			</xsl:for-each>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="max">
	<xsl:param name="nodes" select="/.." />
	<xsl:choose>
		<xsl:when test="not($nodes)">NaN</xsl:when>
		<xsl:otherwise>
			<xsl:for-each select="$nodes">
				<xsl:sort data-type="number" order="descending" />
				<xsl:if test="position() = 1">
					<xsl:value-of select="number(.)" />
				</xsl:if>
			</xsl:for-each>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="percentile">
	<xsl:param name="nodes" select="/.." />
	<xsl:param name="pct" select="0.95"/>
	<xsl:choose>
		<xsl:when test="not($nodes)">NaN</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="pos" >
				<xsl:choose>
					<xsl:when test="count($nodes) = 1">1</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="floor(count($nodes) * $pct)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:for-each select="$nodes">
				<xsl:sort data-type="number"/>
					<xsl:if test="position() = $pos">
						<xsl:value-of select="number(.)" />
					</xsl:if>
			</xsl:for-each>
		</xsl:otherwise>
	</xsl:choose>		
</xsl:template>

<xsl:template name="display-percent">
	<xsl:param name="value" />
	<xsl:value-of select="format-number($value,'0.00%')" />
</xsl:template>

<xsl:template name="display-time">
	<xsl:param name="value" />
	<xsl:value-of select="format-number($value,'0')" />
</xsl:template>

<xsl:template name="display-2dp">
	<xsl:param name="value" />
	<xsl:value-of select="format-number($value,'0.00')" />
</xsl:template>	
</xsl:stylesheet>
